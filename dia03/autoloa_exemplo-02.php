<?php

function incluirClasses($class_name){
    if(file_exists($class_name.".php") === true) {
        require_once ("$class_name.php");
    }
}

spl_autoload_register('incluirClasses');
spl_autoload_register(function ($class_name) {

    if (file_exists('Abstract'. DIRECTORY_SEPARATOR . $class_name . '.php') === true) {
        require_once('Abstract'. DIRECTORY_SEPARATOR . $class_name . '.php');
    }
});

$new = new DelRey();
$new->acelerar(50, "full", 2);