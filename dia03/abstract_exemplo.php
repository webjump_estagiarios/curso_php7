<?php
require_once ("interface_exemplo.php");

abstract class Automovel implements Veiculo {
    public function acelerar($velocidade, $gasolina, $tracao)
    {
        // as variaveis nos parametros podem ser diferentes
        echo "o veículo acelerou até " . $velocidade . " km/h e tem de gasolina ". $gasolina ." p/km com tração de ". $tracao;
    }

    public function frenar($velocidade, $inercia)
    {
        echo "O veiculo frenou com uma velocidade de ". $velocidade . " km/h e inercia de ". $inercia;
    }
    public function trocarMarcha($marcha, $velocidade)
    {
        echo "O veículo engatou a marcha " . $marcha . " com velocidade de ". $velocidade;
    }
}

class DelRey extends Automovel {
    
    public function metodoDaDelRey(){
        
    }
}

$carro = new DelRey();
$carro->acelerar(60, "full", 20);

// Não pode instanciar uma classe abstrata: $carro2 = new Automovel();