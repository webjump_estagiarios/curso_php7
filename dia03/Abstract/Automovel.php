<?php
interface Veiculo {

    public function acelerar($velocidade, $gasolina, $tracao);
    public function frenar ($velocidade, $inercia);
    public function trocarMarcha ($marcha, $velocidade);

}
abstract class Automovel implements Veiculo {
    public function acelerar($velocidade, $gasolina, $tracao)
    {
        // as variaveis nos parametros podem ser diferentes
        echo "o veículo acelerou até " . $velocidade . " km/h e tem de gasolina ". $gasolina ." p/km com tração de ". $tracao;
    }

    public function frenar($velocidade, $inercia)
    {
        echo "O veiculo frenou com uma velocidade de ". $velocidade . " km/h e inercia de ". $inercia;
    }
    public function trocarMarcha($marcha, $velocidade)
    {
        echo "O veículo engatou a marcha " . $marcha . " com velocidade de ". $velocidade;
    }
}