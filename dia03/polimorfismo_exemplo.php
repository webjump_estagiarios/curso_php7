<?php

abstract class Animal {

    public function falar(){
        return " Som";
    }

    public function mover(){
        return " Andar";
    }

}

class Gato extends Animal {
    public function falar()
    {
        return " Miau";
    }
}

class Cachorro extends Animal {
    public function falar()
    {
        return " Au";
    }
}

class Passaro extends Animal {
    public function falar()
    {
        return " Piu piu";
    }

    public function mover()
    {
        return " Voa" . parent::mover();
    }
}

$Sarabi = new Gato();
$Amora  = new Cachorro();
$Preto  = new Passaro();

echo $Preto->mover();
echo $Preto->falar();

echo $Amora->falar();
echo $Amora->mover();

echo $Sarabi->falar();
echo $Sarabi->mover();