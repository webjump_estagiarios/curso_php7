<?php

interface Veiculo {

    public function acelerar($velocidade, $gasolina, $tracao);
    public function frenar ($velocidade, $inercia);
    public function trocarMarcha ($marcha, $velocidade);

}

interface Avioes {

    public function acelerar($velocidade, $gasolina, $tracao);
    public function frenar ($velocidade, $inercia);
    public function trocarMarcha ($marcha, $velocidade);

}

class Civic implements Veiculo, Avioes {
    public function acelerar($velocidade, $gasolina, $tracao)
    {
        // as variaveis nos parametros podem ser diferentes
        echo "o veículo acelerou até " . $velocidade . " km/h e tem de gasolina ". $gasolina ." p/km com tração de ". $tracao;
    }

    public function frenar($velocidade, $inercia)
    {
        echo "O veiculo frenou com uma velocidade de ". $velocidade . " km/h e inercia de ". $inercia;
    }
    public function trocarMarcha($marcha, $velocidade)
    {
        echo "O veículo engatou a marcha " . $marcha . " com velocidade de ". $velocidade;
    }
}
/*
$carro = new Civic();
$carro->acelerar(10, "Full", 3);
$carro->trocarMarcha(2, 30);
$carro->frenar(30, 0);
*/
