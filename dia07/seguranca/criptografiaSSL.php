<?php

define ('SECRET_IV', pack('a16', 'senha'));
define ('SECRET', pack('a16', 'senha'));

$data = [

    "nome"=>"Webjump"

];

$openssl = openssl_encrypt(
    json_encode($data),
    'AES-128-CBC',
    SECRET,
    0,
    SECRET_IV
);

echo base64_encode($openssl);

$string = openssl_decrypt($openssl, 'AES-128-CBC', SECRET, 0, SECRET_IV);

echo $string;  