<?php

//Extrair dados de um arquivo.csv
$filename = "usuarios.csv";

if (file_exists($filename)) {

    $file = fopen($filename, "r");

    //$headers = fgets($file); // fgets pega a primeira linha.

    $headers = explode(",", fgets($file)); // explode mostra os arquivos em array da primeira linha.

    $data = array();

    while ($row = fgets($file)) {

        $rowData = explode(",",$row);

        $linha = array();

        for ($i =0; $i < count($headers); $i++){

            $linha[$headers[$i]] = $rowData[$i];

        }

        array_push($data , $linha);

    }

    fclose($file);

    echo json_encode($data);

}

?>
