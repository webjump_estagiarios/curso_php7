<?php
//variaveis locais só funcionam na página onde foram criadas. Porém, as váriaveis de sessão são visiveis em todo o código enquanto o usúrio estiver conectado no nosso servidor
// sessões duram em média 20m, mas podemos mudar ou matar a sessão

session_start();

$_SESSION["nome"] = "Dauer";

//Ao chamar esse valor usa o session_start(); mas antes é preciso passar por essa página
// O ideal é criar uma página config.php onde ficam as Constantes e as variáveis de sessão, tratamento de erros e etc...
// para apagar variaveis de sessão podemos usar o unset, mas o ideal é usar o session_unset($_SESSION["nome"]); se não passar o indice "" ele apaga todas as variaveis. Também podemos usar o session_destroy(); Ele limpa a variável e retira o úsuario da sessão, ao recarregar a página é como se fosse um novo acesso ao servidor

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                             //Id de sessão //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Quem gera os id são os servidores -> session_id(); retorna o id criado pelo servidor -> valor único entre você e o servidor
// é possível forçar que seja criado uma sessão caso o úsuario não tenha permissão session_regenerate_id(); -> recria a id 
//se um hacker tiver o session id ele tem acesso as informações do cliente, session hijacking 
//https encrypta o id session
// session_id(""); -> recupera uma sessão de uma dado usuário

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                       // Funções Para Session //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//php.net -> Caracteristicas -> sessões -> session references

//session_save_path -> Retorna o caminho de salvamento das sessões

switch(session_status()) {
 
   case PHP_SESSION_DISABLED:
   echo "Sessões estão desabilitadas";
   break;

   case PHP_SESSION_NONE:
   echo "Sessões estão habilitadas, mas não iniciadas";
   break;

   case PHP_SESSION_ACTIVE:
   echo "Sessões estão ativas, e pelo menos uma, existe;";
   break;
}


?>

