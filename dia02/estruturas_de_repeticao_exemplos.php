<?php

echo "<select>";

for ($i=date("Y"); $i >= date("Y")-100; $i--) {

   echo '<option value="'.$i.'">'.$i.'</option>';
}

echo "</select>"

////////////////////////////////////////////////////////////////////////////////////////////////////
// Foreach //

$meses = array(
   "Janeiro", "Favereiro", "Março",
   "Abril",   "Maio",      "Junho",
   "Julho",   "Agosto",    "Setembro", 
   "Outubro", "Novembro",  "Dezembro"
);

foreach ($meses as $mes) { // Só mostra o conteúdo

   echo "O mês é: ".$mes "<br />";
}

foreach ($meses as $index => $mes) { // Mostra o conteúdo e seus respectivos indices

   echo "Indice: ".$index."<br />";
   echo "O mês é :".$mes."<br /><br />";

}
?>

<form>
   <input type="text"   name="nome">
   <input type="date"   name="nascimento">
   <input type="submit" value="ok">
</form>
<?php

if(isset($_GET)) {
   foreach ($_GET as $key => $value) {
   
      echo "Nome do campo: ". $key . "<br />";
      echo "Valor do campo: ". $value;
      echo "<hr>";
   }
}

