<?php

$hierarquia = array(
   array(
   //CEO
   'nome_cargo' => 'CEO',
   'subordinados' => array(
      //Inicio: Diretores
      array(
         'nome_cargo' => 'Diretor Comercial',
         'subordinado' => array(
            //Inicio: Gerente de vendas
            array(
               'nome_cargo' => 'gerente de vendas'
                 )
	   //Termino: Gerente de vendas
           ),
//Termino: Diretor Comercial
//Inicio: Diretor Financeiro
    array(
       'nome_cargo' => 'Diretor Financeiro',
       'subordinados' => array(
          //Inicio: Gerente de Contas a Pagar
          array(
             'nome_cargo' => 'gerente de contas a Pagar',
             'subordinados' => array(
                 //Inicio: Supervisor de pagamentos
                 array(
                    'nome_cargo' => 'Supervisor de pagamentos'
                      )
                     //Termino: Supervisor de pagamentos
                                    )
                ),
           //Termino: Gerente de compras
                              )
          )
          )//Termino Diretor Financeiro
                           )//Termino diretor Comercial
)//CEO
);

function exibe($cargos){

   $html = '<ul>';

   foreach($cargos as $cargo) {
      
      $html .= "<li>";
      $html .= cargo['nome_cargo'];
      
      if(isset($cargo['subordinados']) && count($cargo['subordinados']) > 0){
       
         $html .= exibe($cargo['subordinados']);
      }

      $html .= "</li>";
}

return $html;
}
//Three view
