<?php
include "exemplo01.php";
include_once "exemplo01.php";]

require "exemplo02.php";
require_once "exemplo02.php";

// A diferença entre ambos é a forma como é tratado os erros p.ex: include retorna o erro e continua o restante do script
// Require retorna um FATAL ERROR, ele para a execução do script caso não encontre o arquivo
// ONCE é para incluir apenas uma vez, assim, caso o arquivo seja executado masi de uma vez ele não irá criar outra instancia do arquivo de exemplo

include "inc/exemplo-03.php"; //incluindo com subdiretorios
include "../inc/exemplo-03.php"; //incluindo com diretórios pai, ../ -> volta um diretório. Se precisar subir mais de um diretório talvez seja bom rever a estrutura de pastas
//include remoto, traz o conteúdo de um site -> POUCO SEGURO

//ideal é usar o require ao invés de include

//A partir do php7 ERROR FATAL não impede o script de executar, ele agora retorna uma excessão que pode ser tratada via try catch

?>
