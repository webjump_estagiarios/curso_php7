<?php

$pessoas = array();

//ADICIONA ELEMENTOS NUM ARRAY JÁ CRIADO
array_push($pessoas, array(
   'nome' => 'Gustavo',
   'idade' => 20
));

//PODERIA FAZER TUDO JUNTO
array_push($pessoas, array(
   'nome' => 'Herbert',
   'idade' => 21
));

print_r($pessoas[0]['nome']);

////////////////////////////////////////////////////////////////////////////////////
        // CONSTANTES //
// bom usar como letras maiúsculas//

define("SERVIDOR", "127.0.0.1");

echo SERVIDOR;

//Criando um vetor constante, disponivel apenas no php7.0

define("BANCO_DE_DADOS", [
   '127.0.0.1',
   'root',
   'root',
]);

print_r("BANCO_DE_DADOS");

echo PHP_VERSION . "<br />";
echo DIRECTORY_SEPARATOR;

?>
