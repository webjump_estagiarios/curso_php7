<?php
     class Endereco{

         private $logradouro;
         private $numero;
         private $cidade;

         public function __construct($l, $n, $c){
             $this->logradouro =$l;
             $this->numero =$n;
             $this->cidade =$c;
          
         }

         public function __toString(){
             return $this->logradouro." ," .$this->numero. "," .$this->cidade;
         }
         
             
         

     }

     $meuEndereco = new Endereco("Rua Teresa Stangorlini", "104", "São Bernardo");

     echo $meuEndereco;