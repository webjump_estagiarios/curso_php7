<?php

// retorna um vetor com o conteudo
$images = scandir("images");

$data = array();

// var_dump($images);

foreach ($images as $img) {
    if(!in_array($img, array(".", ".."))) {

        $filename = "images" . DIRECTORY_SEPARATOR . $img;

        $info = pathinfo($filename);

        $info["size"] = filesize($filename);
        
        $info["modified"] = date("d/m/Y H:i:s", filemtime($filename));

        $info["url"] = "/var/www/git/gitBucket/curso_php7/dia04/arquivos/".str_replace("\\", "/", $filename);



        //var_dump($info);
        
        array_push($data, $info);
    }
}

echo json_encode($data);
