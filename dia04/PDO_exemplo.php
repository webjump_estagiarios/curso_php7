<?php
$conn = new PDO("mysql:dbname=dbphp7; host=localhost", "root", "root");
//sqlserver -> $conn = new PDO("sqlsrv:Database=dbphp7;server=localhost\SQLEXPRESS;ConnectionPooling=0, "root", "root);
//Pooling = 0 executa e fecha a conexão. Com valor 1 = multi thread, consegue executar msm q a página seja alterada

$stmt = $conn->prepare("SELECT * FROM tb_usr ORDER BY deslogin");

$stmt->execute();

//var_dump($stmt->fetchAll(PDO::FETCH_ASSOC));
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($result as $row) { //exibindo toda as informações para o front

    foreach ($row as $key => $value) {

        echo "<strong>" . $key . ": </strong>" . $value . "<br/>";

    }

    echo "==================================================== <br/>";
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Inserindo dados com insert via PDO ///

$stmt = $conn->prepare("INSERT INTO tb_usr (deslogin, despassword) VALUES (:LOGIN, :PASSWORD)");

$login    = 'Jose';
$password = 'senha123456789';

$stmt->bindParam(":LOGIN", $login);
$stmt->bindParam(":PASSWORD", $password);

$stmt->execute();

echo "Iserido, taokey";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Atualizando Dados com Update via PDO ///

$stmt = $conn->prepare("UPDATE tb_usr SET deslogin = :LOGIN, despassword = :PASSWORD WHERE idusr = :ID");

$login    = 'Josivaldo';
$password = 'senhaJosi';
$id       = 2;

$stmt->bindParam(":LOGIN", $login);
$stmt->bindParam(":PASSWORD", $password);
$stmt->bindParam(":ID", $id);

$stmt->execute();

echo "Atualizado, taokey";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Deletando Dados com Delete via PDO ///

$stmt = $conn->prepare("DELETE FROM tb_usr WHERE idusr = :ID");

$id = 5;

$stmt->bindParam(":ID", $id);

$stmt->execute();

echo "Deletado, taokey";
