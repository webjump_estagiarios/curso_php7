<?php

abstract class Animal{
    public function falar(){
        return "Som";
    }
    public function mover(){
        return"Anda";
    }
}

class Cachorro extends Animal{
    public function falar(){
        return "Late";
    }
}

class Gato extends Animal{
    public function falar(){
        return "Mial";
    }
}

class Passaro extends Animal{
    public function falar(){
        return "Canta";
    }
    public function mover(){
        return "Voa e"  .  parent :: mover();
    }
}

$cat = new Gato();
echo $cat->falar() . "<br>";
echo $cat->mover() . "<br>";

echo"-----------------------------</br>";

$dog = new Cachorro();
echo $dog->falar() . "<br>";
echo $dog->mover() . "<br>";

echo"-----------------------------</br>";

$ave = new Passaro();
echo $ave->falar() . "<br>";
echo $ave->mover() . "<br>";

