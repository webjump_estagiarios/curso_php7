<?php
class Sql extends PDO {

    private $conn;

    public function __construct()
    {
        $this->conn = new PDO("mysql:host=localhost;dbname=dbphp7", "root", "root");
    }


    private function setParams($stmt, $params = array()) { // faz n bind params para params > 1

        foreach ($params as $key => $value) {

            $this->setParam($stmt, $key, $value);

        }
    }

    private function setParam($stmt, $key, $value) { // faz o bind params

        $stmt->bindParam($key, $value);

    }

    public function query ($rawQuery, $params = array()) { // faz o tratamento da rawQuery

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        $stmt->execute();

        return $stmt;

    }

    public function select ($rawQuery, $params = array()):array { // Equivalente ao método getList da classe usúario

        $stmt = $this->query($rawQuery, $params);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}