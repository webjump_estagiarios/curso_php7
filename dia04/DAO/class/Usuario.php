<?php
class Usuario {

private $idusr;
private $deslogin;
private $despassword;
private $dtcadastro;

public function __construct($login='', $password='')
{
    $this->setLogin($login);
    $this->setSenha($password);
}

    public function getId() {
    return $this->idusr;
}

public function setId($idusr) {
    $this->idusr = $idusr;
}

public function getLogin() {
    return $this->deslogin;
}

public function setLogin($deslogin) {
    $this->deslogin = $deslogin;
}

public function getPassword() {
    return $this->despassword;
}

public function setSenha($despassword) {
    $this->despassword = $despassword;
}

public function getDtcadastro() {
    return $this->dtcadastro;
}

public function setDtcadastro($dtcadastro) {
    $this->dtcadastro = $dtcadastro;
}

public function loadById($id) {
    $sql = new Sql();

    $result = $sql->select("SELECT * FROM tb_usr WHERE idusr = :ID", array(
       ":ID"=>$id
    ));

    if(isset($result[0])) {

        $this->setData($result[0]);
    }
}

public static function search($login) {
    $sql = new Sql();

    return $sql->select("SELECT * FROM tb_usr WHERE deslogin LIKE :SEARCH ORDER BY deslogin", array(
        ":SEARCH"=>"%".$login."%"
    ));
}

public static function getList() {
    $sql = new Sql();

    return $sql->select("SELECT * FROM tb_usr ORDER BY deslogin");
}

public function login($login, $password) {
    $sql = new Sql();

    $result = $sql->select("SELECT * FROM tb_usr WHERE deslogin = :LOGIN AND despassword = :PASSWORD", array(
        ":LOGIN"=>$login,
        ":PASSWORD"=>$password
    ));

    if(isset($result[0])) {

        $this->setData($result[0]);

    } else {

        throw new Exception("Login ou senha incorretos bixo");

    }

}

public function update($login, $password) {

    $this->setLogin($login);
    $this->setSenha($password);

    $sql = new Sql();

    $sql->query("UPDATE tb_usr SET deslogin = :LOGIN, despassword = :PASSWORD WHERE idusr = :ID", array(
       ':LOGIN'=>$this->getLogin(),
       ':PASSWORD'=>$this->getPassword(),
       ':ID'=>$this->getId()
    ));
}

public function insert () {
    $sql = new Sql;

    $results = $sql->select("CALL sp_usuarios_insert(:LOGIN, :PASSWORD)", array( //retorna o id do usr da tabela
        ':LOGIN'=>$this->getLogin(),
        ':PASSWORD'=>$this->getPassword()
    ));

    if(isset($results)) {
        $this->setData($results[0]);
    }
}

public function delete () {
    $sql = new Sql();

    $sql->query("DELETE FROM tb_usr WHERE idusr = :ID", array(

        ':ID'=>$this->getId()

    ));

    $this->setId(0);
    $this->setLogin("");
    $this->setSenha("");
    $this->setDtcadastro(new DateTime());
}

public function setData($data) {

    $this->setId($data['idusr']);
    $this->setLogin($data['deslogin']);
    $this->setSenha($data['despassword']);
    $this->setDtcadastro(new DateTime($data['dtcad']));

}

public function __toString()
{
    return json_encode(array(
       "idusr"=>$this->getId(),
       "deslogin"=>$this->getLogin(),
       "despassword"=>$this->getPassword(),
       "dtcad"=>$this->getDtcadastro()->format("d/m/Y H:i:s")
    ));
}
}