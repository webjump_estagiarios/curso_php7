<?php
require_once ("config.php");

$sql = new Sql();

$usr = $sql->select("SELECT * FROM tb_usr ORDER BY deslogin");

$headers = array();

foreach ($usr[0] as $key => $value) {
    array_push($headers, ucfirst($key));
}

$file = fopen("usuarios.csv", "w+");

fwrite($file, implode(';', $headers) . "\r\n");

foreach ($usr as $row) { // nas linhas, nos registros

    $data = array();

    foreach ($row as $key => $value) { // nos campos, colunas [i][j]

        array_push($data, $value);

    } // END coluna

    fwrite($file, implode(';', $data) . "\r\n");

}// END linha

fclose($file);

//echo implode(";", $headers);