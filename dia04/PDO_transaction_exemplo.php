<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Usando transações ///

// a transação é atrelada a conexão e não ao stmt

$conn = new PDO("mysql:dbname=dbphp7; host=localhost", "root", "root");

$conn->beginTransaction();

$stmt = $conn->prepare("DELETE FROM tb_usr WHERE idusr = ?");

$id = 7;

$stmt->execute(array($id));

//$conn->rollBack(); // volta a transação

var_dump($conn->commit());

echo "Deletado, taokey";