# Curso completo PHP7.0 #

## Introdução ##
Este Curso de PHP 7 Completo vai proporcionar em um só lugar tudo o que precisa saber para criar seu próprio website. A equipe de instrutores da Hcode estará disponível para responder suas dúvida. 

Neste curso cobriremos os seguintes tópicos:

  - Aprenda em um curso completo a linguagem PHP, e sua nova versão 7, aprenda o que mudou em relação ao PHP 5.6 além de boas práticas e os comandos mais importantes. Mesmo que você não tenha conhecimento algum e nunca tenha programado, conseguirá acompanhar com inúmeros exemplos e dicas dos Instrutores que atuam a mais de 10 anos com PHP. 2) Aprenda a criar um sistema administrativo de uma loja virtual gerando boleto bancário. 3) Trabalhe com Banco de Dados, Segurança. 4) Aprenda a criar um recursos incríveis, utilizando PHP e MySQL.

  - Ao final deste curso PHP 7 você estará apto a criar sites modernos e completos, poderá se diferenciar no mercado de trabalho e até mesmo começar uma nova carreira de desenvolvedor web.

  - Acesso vitalício! Te aguardo no curso!
  
 -----

### O que você aprenderá? ###
Com este curso completo que aborda conceitos, e exemplos práticos você aprenderá com dois especialistas como desenvolver projetos reais.

Ao final do Curso, os alunos que assistirem todas as aulas, realizarem os testes, exercícios e acompanharem os projetos desenvolvido com os instrutores, estarão aptos a desenvolverem sites e sistemas utilizando uma linguagem robusta e completa que é o PHP 7, além de aprenderem boas práticas e analisarem mudanças ocorridas na linguagem.
Há algum requisito ou pré-requisito para o curso?

É importante que os alunos tenham conhecimentos básicos de HTML e CSS, usaremos o HTML5 e o CSS3, mas não explicaremos a fundo essas tecnologias.

É desejável que os alunos conheçam Lógica de Programação, caso não o saibam poderão adquirir o curso, mas será melhor aproveitado com este conhecimento prévio.

### Para quem é este curso: ###
* Este curso de PHP destina-se aos que desejam se tornar programadores ou desenvolvedores web profissionais utilizando a linguagem PHP.
* Estudantes de Cursos voltados a Tecnologia da Informação ou Interessados em ingressar na área de Tecnologia da Informação, e mais especificamente no mercado de Desenvolvimento Web.
* Front-End Developer que desejam conhecer o mundo Back-End, além de se aprofundarem em Programação PHP. Absorvendo conhecimento sobre uma das linguagens de programação mais populares do mundo.

-----

## Instalação ##
Para acompanhar o curso sera preciso ter os seguintes recursos instalados:

* Mysql
* PHP 7.0
* Virtual host, nginx ou apache (preferencia nginx)
* (opcional) IDE

## Configurações ##
* Mysql: https://www.mysql.com/products/workbench/
* PHP 7.0: https://sempreupdate.com.br/como-instalar-versoes-diferentes-do-php-5-6-7-0-e-7-1-no-ubuntu/
* Virtual Host (instalação): https://www.digitalocean.com/community/tutorials/como-instalar-o-nginx-no-ubuntu-16-04-pt
* Virtual Host (configuração): http://manjaro-linux.com.br/forum/dicas-truques/como-configurar-virtual-hosts-server-blocks-no-nginx-t4277.html 

-----

## Arquivos Nota ##
Os arquivos com nome de Nota.txt são informações adicionais dos professores do curso, convem ler o seu conteúdo sempre que se deparar com uma delas.

-----

## Com quem falar ###

* Luan Alves
* Alexandre Rodrigues

-----
## Link do Curso ##
* https://www.udemy.com/curso-php-7-online/learn/v4/overview