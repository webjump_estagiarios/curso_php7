<?php

//viacep webservice gratuito

$cep = "03735120";
$link = "https://viacep.com.br/ws/$cep/json/";


$ch = curl_init($link);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // a constante pode me retornar ou não a resposta de volta
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

$response = curl_exec($ch);

curl_close($ch);

$data = json_decode($response, true); // true transforma em array não em objeto

print_r($data);
?>