<?php
// Graphic Designer Library, gerar images dinâmicamente como certificados etc.
/*
 * Podemos trabalhar com os eixo x e y
 * Podemos gerar thumbnail automáticamente, dessa forma diminuimos os bytes de uma imagem
 * Jquery pode se comunicar com o php para redimensionar imagens
 * Posso criar um arquivo php q pode ser chamado pela tag img e criar uma imagem no html
 * Colocamos o header por último (ou comentado) para podermos tratar os erros
 *
 */

header("Content-type: image/png");

$image = imagecreate(256, 256);  //minha fariável resource

// a primeira cor que você cria é automáticamente a cor de fundo

$black = imagecolorallocate($image, 0, 0, 0);
$red   = imagecolorallocate($image, 255, 0, 0);

//escrever na tela imagestring -> mais simples
imagestring($image,  7, 60, 120, "Curso de PHP 7", $red);

// definindo o formato, equivalente ao header

imagepng($image);

imagedestroy($image);