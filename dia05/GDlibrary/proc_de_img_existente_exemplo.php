<?php

$image = imagecreatefromjpeg("certificado.jpg");

$titleColor = imagecolorallocate($image, 0, 0, 0);
$gray       = imagecolorallocate($image, 100, 100, 100);

imagestring($image, 9, 350, 150, "CERTIFICADO WEBJUMP", $titleColor);
imagestring($image, 6, 340, 350, "Gustavo Vicente Dauer", $titleColor);
imagestring($image, 3, 340, 370,utf8_decode("Concluído em: ") . date("d/m/Y H:i:s"), $titleColor);

header("Content-type: image/jpeg");

imagejpeg($image, "certificado-" . date("Y-m-d") . ".jpg", 10); //baixar no servidor
imagejpeg($image); //exibe na tela

imagedestroy($image);
