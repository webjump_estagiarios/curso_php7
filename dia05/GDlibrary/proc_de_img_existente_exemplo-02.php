<?php

$image = imagecreatefromjpeg("certificado.jpg");

$titleColor = imagecolorallocate($image, 0, 0, 0);
$gray       = imagecolorallocate($image, 100, 100, 100);

imagettftext($image, 42, 0, 320, 250, $titleColor, "fonts" . DIRECTORY_SEPARATOR . "Bevan" . DIRECTORY_SEPARATOR . "Bevan-Regular.ttf", "CERTIFICADO");
imagettftext($image, 32, 0, 375, 350, $titleColor, "fonts" . DIRECTORY_SEPARATOR . "Playball" . DIRECTORY_SEPARATOR . "Playball-Regular.ttf", "Gustavo Vicente Dauer");
imagestring($image, 3, 450, 370,utf8_decode("Concluído em: ") . date("d/m/Y H:i:s"), $titleColor);

header("Content-type: image/jpeg");

//imagejpeg($image, "certificado-" . date("Y-m-d") . ".jpg", 10); //baixar no servidor
imagejpeg($image); //exibe na tela

imagedestroy($image);
