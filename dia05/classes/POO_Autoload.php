<?php

interface Car{
    public function vrum($velocidade);
    public function frenar ($velocidade);
    public function trocarMarcha($marcha);

}

abstract Class Altomovel implements Car {

    public function vrum($velocidade){
        echo "O veículo acelerou até " . $velocidade . "km/h";
    }
    public function frenar($velocidade){
        echo "O veículo frenou até" . $velocidade . "km/h";
    }

    public function trocarMarcha($marcha){
        echo "O veiculo engatou a marcha" . $marcha;
    }


}


?>