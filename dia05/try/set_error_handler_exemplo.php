<?php
// tratando um cabeçalho de erros
// no conf.php deixariamos as tratações de erro

/*
 * error_reporting(E_ALL & ~E_NOTICE); Reescreve o php.ini para não permitir q as notice apareçam para NINGUÉM
 */

function error_handler ($code, $message, $file, $line) {

    echo json_encode(array(
        "code"   => $code,
        "message"=> $message,
        "file"   => $file,
        "line"   => $line
    ));
}

set_error_handler("error_handler");

echo 100/0;