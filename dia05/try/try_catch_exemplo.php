<?php

try {

    throw new Exception("Houve um erro, taokey", 400);
    //devolver erro em json para o fron-end consumir e fazer o tratamento visual

} catch (Exception $e) {

    echo json_encode(array(

        "message"=>$e->getMessage(),
        "line"   =>$e->getLine(),
        "file"   =>$e->getCode()

    ));

}