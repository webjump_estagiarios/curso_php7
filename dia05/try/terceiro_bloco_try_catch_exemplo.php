<?php
function trataNome($nome)
{

    if (!$nome) {

        throw new Exception ("Nenhum nome foi informado. ", 1);

    }
}

    echo ucfirst($nome . "<br />");

    try {

       trataNome("João");
       trataNome("");

    } catch (Exception $e) {

        echo $e->getMessage();

    } finally { // poderia mandar um e-mail para o adm mostradno como o sistema se comportou

        echo "<br /> Executou o bloco try";

    }
