<?php

/* Aula PHP: Iniciante
 tipos de variaveis */

$nome = "Gustavo Dauer";

//imprime
echo $nome;

//imprime toda a informação dos dados
var_dump($nome);

//Camuel Case: Padrão de nomeclatura, começa com letra minúscula e a segunda maiúscula p.ex:
$anoNascimento = 1980;
$nomeCompleto  = "Gustavo Vicente Dauer";

/*Nunca inicie uma variável com número ou caractere especiais (com excessão do _(underline))
$123nome;
$@#!#nome;
*/

//apagando variavel
unset($nome);

//Perguntando se a variável existe
if(isset($nome))
{
   //bloco de comandos
}

//concatenação (juntar)
$nome	      = "Gustavo";
$sobrenome    = "Vicente Dauer";
$nomeCompleto = $nome . " " . $sobrenome;

//parar a execução do PHP
exit;

//php7 não possui conversão automática p.ex: João10 + 30 = 40

$data    = new DateTime();//vazio retorna a data atual, é um tipo combosto: array e objeto
$arquivo = fopen("arquivo.extenção", "tipo de leitura p.ex: r"); // abrindo arquivo

$nulo  = NULL; //nulo ausencia de valor -> p.ex: espaço não tem oxigênio
$vazio = ""; //Falta de valor, ela existe mas não tem informação -> p.ex: espaço aério

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// VARIAVEIS PRÉ DEFINIDAS OU SUPERGLOBAIS -> podem ser acessadas de qualquer local do php

$nome = $_GET["nome"]; //pega as informações via URL p.ex: localhost/arquivo.php ? nome = Gustavo Dauer & idade = 99
//convertendo doados $nome = (int)$_GET["nome"];

//pegando o ip do ambiente -> Usuário e Servidor
$ip = $_SERVER["REMOTE_ADDR"];

//Para criar arquivos de log -> retorna onde o script ta sendo executado
$ip = $_SERVER["SCRIPT_NAME"];

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Escopo de variáveis -> Significa, até onde uma determminada variavels será enxergada

{ //Isso é um escopo


}

//exemplo prático
function test(){

 echo "teste"

}
?>
